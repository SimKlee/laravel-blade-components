<div {{ $attributes->merge(['class' => 'card bg-light']) }}>
    {{ $slot }}
</div>
