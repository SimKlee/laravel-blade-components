@extends($extends)
@section('element'.$id)
    <input type="{{ $type }}"
           id="{{ $id }}"
           name="{{ $id }}"
           value="{{ $value }}"
           {{ $attributes->merge(['class' => 'form-control']) }}
           @if($placeholder) placeholder="{{ $placeholder }}" @endif>
@endsection