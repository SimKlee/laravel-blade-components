<x-lbc::card.card>
    <x-lbc::card.header>{{ $title }}</x-lbc::card.header>
    <x-lbc::card.body>
        @include('lbc::components.form.form')
    </x-lbc::card.body>
    <x-lbc::card.footer class="d-flex flex-row-reverse">
        <button type="submit" name="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Save</button>&nbsp;
        <a href="{{ $backLink }}" class="btn btn-sm btn-secondary"><i class="fa fa-cancel"></i> Cancel</a>
    </x-lbc::card.footer>
</x-lbc::card.card>
