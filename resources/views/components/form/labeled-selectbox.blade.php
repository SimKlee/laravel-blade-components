@extends($extends)
@section('element'.$id)
    <select id="{{ $id }}"
            name="{{ $id }}"
            {{ $attributes->merge(['class' => 'form-select']) }}>
        @if($emptyOption)
            <option></option>
        @endif
        @foreach($options as $optionValue => $optionLabel)
            <option value="{{ $optionValue }}"
                    @if($optionValue == $value || $optionValue === $default) selected @endif>{{ $optionLabel }}</option>
        @endforeach
    </select>
@endsection