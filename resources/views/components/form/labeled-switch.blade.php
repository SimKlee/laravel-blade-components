@extends($extends)
@section('element'.$id)
    <div class="form-check form-switch">
        <input class="form-check-input" type="checkbox" id="{{ $id }}" name="{{ $id }}" @if($value == true) checked @endif value="1">
    </div>
@endsection