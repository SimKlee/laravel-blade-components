@extends($extends)
@section('element'.$id)
    <textarea id="{{ $id }}"
              name="{{ $id }}"
              {{ $attributes->merge(['class' => 'form-control']) }}
              @if($placeholder) placeholder="{{ $placeholder }}" @endif>{{ $value }}</textarea>
@endsection