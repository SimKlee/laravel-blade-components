<form method="{{ $method }}" @if($action) action="{{ $action }}" @endif @if($multipart) enctype="multipart/form-data" @endif {{ $attributes }}>
    @if($csrf) @csrf @endif
    @if($redirect) <input type="hidden" name="redirect" value="{{ $redirect }}"> @endif
    {{ $slot }}
</form>
