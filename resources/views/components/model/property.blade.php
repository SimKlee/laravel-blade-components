@if($lookup)
    <x-lbc::form.labeled-selectbox id="{{ $property }}"
                                   label="{{ $label }}"
                                   value="{{ $value }}"
                                   empty-option="true"
                                   :options="$lookupValues"/>
@else
    @switch($type)

        @case('string')
        <x-lbc::form.labeled-textbox id="{{ $property }}"
                                     type="text"
                                     label="{{ $label }}"
                                     maxlength="{{ $maxlength }}"
                                     value="{{ $value }}"
                                     {{ $attributes }}/>
        @break

        @case('text')
        <x-lbc::form.labeled-textarea id="{{ $property }}"
                                      type="text"
                                      label="{{ $label }}"
                                      maxlength="{{ $maxlength }}"
                                      value="{{ $value }}"
                                      {{ $attributes }}/>
        @break

        @case('int')
        <x-lbc::form.labeled-textbox id="{{ $property }}"
                                     type="number"
                                     min="0"
                                     label="{{ $label }}"
                                     maxlength="{{ $maxlength }}"
                                     value="{{ $value }}"
                                     {{ $attributes }}/>
        @break

        @case('Carbon')
        <x-lbc::form.labeled-textbox id="{{ $property }}"
                                     type="date"
                                     min="0"
                                     label="{{ $label }}"
                                     maxlength="{{ $maxlength }}"
                                     value="{{ $value }}"
                                     {{ $attributes }}/>
        @break

        @case('boolean')
        <x-lbc::form.labeled-switch id="{{ $property }}"
                                    label="{{ $label }}"
                                    value="{{ $value }}"
                                    {{ $attributes }}/>
        @break

        @default
        <span class="badge bg-danger">Unknown type {{ $type }}</span>
    @endswitch
@endif
