<table {{ $attributes->merge(['class' => 'table']) }}>
    @if($header ?? false) <thead>{{ $header }}</thead> @endif
    <tbody>{{ $body ?? null }}</tbody>
    @if($footer ?? false) <tfoot>{{ $footer }}</tfoot> @endif
</table>