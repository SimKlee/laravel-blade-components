<x-lbc::card.card>
    <x-lbc::card.header>
        <div class="row align-items-center">
            <div class="col">
                <h4>{{ $title }}</h4>
            </div>
            <div class="col col-auto">{{ $actions ?? null }}</div>
        </div>
    </x-lbc::card.header>
    @if($filter ?? false)
        <x-lbc::card.body class="filter">{{ $filter }}</x-lbc::card.body>
    @endif
    <x-lbc::card.body>
        <table {{ $attributes->merge(['class' => 'table']) }}>
            @if($header ?? false)
                <thead>{{ $header }}</thead>
            @endif

            <tbody>{{ $body ?? null }}</tbody>
            @if($footer ?? false)
                <tfoot>{{ $footer }}</tfoot>
            @endif
        </table>
    </x-lbc::card.body>
    @if($pagination)
    <x-lbc::card.footer>
        <div class="d-flex justify-content-center bg-light">{!! $pagination ?? null !!}</div>
    </x-lbc::card.footer>
    @endif
</x-lbc::card.card>
