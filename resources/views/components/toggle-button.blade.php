<button {{ $attributes->merge(['class' => 'btn']) }} data-bs-toggle="collapse" data-bs-target="#{{ $target }}">
    {{ $slot }}
</button>