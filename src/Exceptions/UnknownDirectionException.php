<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\Exceptions;

use Exception;

class UnknownDirectionException extends Exception
{

}