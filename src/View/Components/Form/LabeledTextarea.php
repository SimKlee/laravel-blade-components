<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\View\Components\Form;

class LabeledTextarea extends LabeledTextbox
{
    public function template(): string
    {
        return 'lbc::components.form.labeled-textarea';
    }

    public static function name(): string
    {
        return 'form.labeled-textarea';
    }

}
