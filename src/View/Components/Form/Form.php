<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;
use SimKlee\LaravelBladeComponents\View\Components\AbstractComponent;

class Form extends AbstractComponent
{
    public string      $method;
    public string|null $action;
    public bool        $csrf;
    public bool        $multipart;
    public string|null $redirect;

    public function __construct(string      $method = 'post',
                                string      $action = null,
                                string|bool $csrf = true,
                                string|bool $multipart = false,
                                string      $redirect = null)
    {
        $this->method    = $method;
        $this->action    = $action;
        $this->csrf      = $this->getBooleanValue($csrf);
        $this->multipart = $this->getBooleanValue($multipart);
        $this->redirect  = $redirect;
    }

    public function render(): View|Closure|string
    {
        return view('lbc::components.form.form');
    }

    public static function name(): string
    {
        return 'form.form';
    }

}
