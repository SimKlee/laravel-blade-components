<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Str;
use SimKlee\LaravelBladeComponents\Exceptions\UnknownDirectionException;
use SimKlee\LaravelBladeComponents\View\Components\AbstractComponent;

abstract class AbstractLabeledElement extends AbstractComponent
{
    public string      $id;
    public string      $label;
    public string|null $value;
    public string      $direction;
    public int         $inputSize;
    public int         $labelSize;
    public string|null $help;
    public string      $align;

    public function __construct(string $id,
                                string $label,
                                string $value = null,
                                string $direction = 'h',
                                string $size = '3/9',
                                string $help = null,
                                string $align = 'left')
    {
        $this->id        = $id;
        $this->label     = $label;
        $this->value     = $value;
        $this->help      = $help;
        $this->direction = $direction;
        $this->align     = $align;
        if (Str::contains($size, '/')) {
            [$label, $input] = explode('/', $size);
            $this->inputSize = (int) $input;
            $this->labelSize = (int) $label;
        }
    }

    /**
     * @throws UnknownDirectionException
     */
    public function render(): View|Closure|string
    {
        $extends = match ($this->direction) {
            'h' => 'lbc::components.form.abstract-labeled-element-h',
            'v' => 'lbc::components.form.abstract-labeled-element-v',
            default => throw new UnknownDirectionException($this->direction)
        };

        return view($this->template())
            ->with('extends', $extends);
    }

    abstract public function template(): string;

    abstract public static function name(): string;
}
