<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Str;
use SimKlee\LaravelBladeComponents\Exceptions\UnknownDirectionException;
use SimKlee\LaravelBladeComponents\View\Components\AbstractComponent;

class LabeledSelectbox extends AbstractLabeledElement
{
    public string|null $default;
    public array       $options;
    public bool        $emptyOption;

    public function __construct(string      $id,
                                string      $label,
                                string      $value = null,
                                string      $direction = 'h',
                                string      $size = '3/9',
                                string      $help = null,
                                string      $align = 'left',
                                array       $options = [],
                                bool|string $emptyOption = true,
                                string      $default = null)
    {
        parent::__construct($id, $label, $value, $direction, $size, $help, $align);

        $this->default     = $default;
        $this->options     = $options;
        $this->emptyOption = $this->getBooleanValue($emptyOption);
    }

    public function template(): string
    {
        return 'lbc::components.form.labeled-selectbox';
    }

    public static function name(): string
    {
        return 'form.labeled-selectbox';
    }

}
