<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Str;
use SimKlee\LaravelBladeComponents\Exceptions\UnknownDirectionException;
use SimKlee\LaravelBladeComponents\View\Components\AbstractComponent;

class LabeledSwitch extends AbstractLabeledElement
{
    public string|null $placeholder;

    public function __construct(string $id,
                                string $label,
                                string $value = null,
                                string $direction = 'h',
                                string $size = '3/9',
                                string $help = null,
                                string $align = 'left',
                                string $placeholder = null)
    {
        parent::__construct($id, $label, $value, $direction, $size, $help, $align);

        $this->placeholder = $placeholder;
    }

    public function template(): string
    {
        return 'lbc::components.form.labeled-switch';
    }

    public static function name(): string
    {
        return 'form.labeled-switch';
    }

}
