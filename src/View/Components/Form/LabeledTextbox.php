<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\View\Components\Form;

use Carbon\Carbon;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Str;
use SimKlee\LaravelBladeComponents\Exceptions\UnknownDirectionException;
use SimKlee\LaravelBladeComponents\View\Components\AbstractComponent;

class LabeledTextbox extends AbstractLabeledElement
{
    public string|null $placeholder;
    public string      $type;

    public function __construct(string $id,
                                string $label,
                                string $value = null,
                                string $direction = 'h',
                                string $size = '3/9',
                                string $help = null,
                                string $align = 'left',
                                string $type = 'text',
                                string $placeholder = null)
    {
        parent::__construct($id, $label, $value, $direction, $size, $help, $align);

        $this->placeholder = $placeholder;
        $this->type        = $type;

        if ($this->type === 'date' && !empty($this->value)) {
            $this->value = Carbon::parse($this->value)->format('Y-m-d');
        } elseif ($this->type === 'datetime' && !empty($this->value)) {
            $this->value = Carbon::parse($this->value)->format('Y-m-d\Th:i');
        }
    }

    public function template(): string
    {
        return 'lbc::components.form.labeled-textbox';
    }

    public static function name(): string
    {
        return 'form.labeled-textbox';
    }

}
