<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;
use SimKlee\LaravelBladeComponents\View\Components\AbstractComponent;

class CardForm extends Form
{
    public string      $method;
    public string|null $action;
    public bool        $csrf;
    public bool        $multipart;
    public string|null $redirect;
    public string|null $title;
    public string      $backLink;

    public function __construct(string      $method = 'post',
                                string      $action = null,
                                string|bool $csrf = true,
                                string|bool $multipart = false,
                                string      $redirect = null,
                                string      $title = null,
                                string      $backLink = '')
    {
        parent::__construct($method, $action, $csrf, $multipart, $redirect);

        $this->title    = $title;
        $this->backLink = $backLink;
    }

    public function render(): View|Closure|string
    {
        return view('lbc::components.form.card-form');
    }

    public static function name(): string
    {
        return 'form.card-form';
    }

}
