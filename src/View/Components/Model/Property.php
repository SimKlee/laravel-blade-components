<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\View\Components\Model;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use ReflectionClass;
use SimKlee\LaravelBladeComponents\View\Components\AbstractComponent;

class Property extends AbstractComponent
{
    public Model|string $model;
    public string       $property;
    public string       $label;
    public mixed        $value;
    public string       $type;
    public int|null     $maxlength;
    public bool         $disabled     = false;
    public bool         $readonly     = false;
    public bool         $lookup       = false;
    public ?string      $lookupLabel  = null;
    public array        $lookupValues = [];

    public function __construct(Model|string $model,
                                string       $property,
                                string       $label = null,
                                string       $value = null,
                                string       $type = 'string',
                                string       $maxlength = null,
                                bool         $disabled = false,
                                bool         $readonly = false,
                                bool         $lookup = false,
                                string       $lookupLabel = null)
    {
        $this->model       = $model;
        $this->property    = $property;
        $tableName         = $this->model instanceof Model
            ? Str::lower(Str::snake(Str::plural(class_basename($this->model))))
            : Str::lower(Str::snake(Str::plural($this->model)));
        $this->label       = $label ?? trans(sprintf('%s.properties.%s', $tableName, $this->property));
        $this->value       = $value ?? ($this->model instanceof Model) ? $this->model->{$this->property} : old($this->property);
        $this->type        = $type;
        $this->maxlength   = is_string($maxlength) ? (int) $maxlength : null;
        $this->disabled    = $disabled;
        $this->readonly    = $readonly;
        $this->lookup      = $lookup;
        $this->lookupLabel = $lookupLabel ?? 'id';

        if ($this->lookup) {
            if (Str::endsWith($this->property, '_id')) {
                $this->fetchLookupValuesFromDatabase();
            } else {
                $this->fetchLookupValuesFromConstants();
            }
        }
    }

    private function fetchLookupValuesFromDatabase(): void
    {
        $this->lookupValues = DB::table(Str::plural(Str::replace('_id', '', $this->property)))
                                ->select([
                                    'id AS id',
                                    $this->lookupLabel . ' AS label',
                                ])
                                ->get()
                                ->mapWithKeys(function (\stdClass $result) {
                                    return [$result->id => $result->label];
                                })->toArray();
    }

    private function fetchLookupValuesFromConstants(): void
    {
        /** @var Model $model */
        $model = $this->model instanceof Model
            ? $this->model
            : sprintf('App\Models\%s', $this->model);

        $tableName = $this->model instanceof Model
            ? Str::lower(Str::snake(Str::plural(class_basename($this->model))))
            : Str::lower(Str::snake(Str::plural($this->model)));

        $needle             = Str::upper($this->property) . '_';
        $reflection         = new ReflectionClass($model);
        $this->lookupValues = collect($reflection->getConstants())
            ->filter(function (mixed $value, string $name) use ($needle) {
                return Str::startsWith(haystack: $name, needles: $needle);
            })->mapWithKeys(function (mixed $value) use ($tableName) {
                return [$value => trans(sprintf('%s.values.%s.%s', $tableName, $this->property, $value))];
            })
            ->toArray();
    }

    public function render(): View|Closure|string
    {
        return view('lbc::components.model.property');
    }

    public static function name(): string
    {
        return 'model.property';
    }
}
