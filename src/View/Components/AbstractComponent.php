<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

abstract class AbstractComponent extends Component
{
    abstract public static function name(): string;

    abstract public function render(): View|Closure|string;

    protected function getBooleanValue($value): bool
    {
        if (is_string($value)) {
            $value = strtolower($value);
        }

        return match ($value) {
            true, 'true', '1', 1, 'yes', 'on' => true,
            false, 'false', '0', 0, 'no', 'off' => false,
        };
    }
}
