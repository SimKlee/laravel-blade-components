<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\View\Components\Card;

use Closure;
use Illuminate\Contracts\View\View;
use SimKlee\LaravelBladeComponents\View\Components\AbstractComponent;

class Body extends AbstractComponent
{
    public function __construct()
    {

    }

    public function render(): View|Closure|string
    {
        return view('lbc::components.card.body');
    }

    public static function name(): string
    {
        return 'card.body';
    }
}