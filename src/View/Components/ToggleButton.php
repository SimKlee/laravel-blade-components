<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\View\Components;

use Closure;
use Illuminate\Contracts\View\View;

class ToggleButton extends AbstractComponent
{
    public string $target;

    public function __construct(string $target)
    {
        $this->target = $target;
    }

    public static function name(): string
    {
        return 'toggle-button';
    }

    public function render(): View|Closure|string
    {
        return view('lbc::components.toggle-button');
    }
}