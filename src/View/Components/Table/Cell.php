<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\View\Components\Table;

use Closure;
use Illuminate\Contracts\View\View;
use SimKlee\LaravelBladeComponents\View\Components\AbstractComponent;

class Cell extends AbstractComponent
{
    public string|bool $nowrap;

    public function __construct(string|bool $nowrap = false)
    {
        $this->nowrap = $this->getBooleanValue($nowrap);
    }

    public function render(): View|Closure|string
    {
        return view('lbc::components.table.cell');
    }

    public static function name(): string
    {
        return 'table.cell';
    }
}
