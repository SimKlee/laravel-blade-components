<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\View\Components\Table;

use Closure;
use Illuminate\Contracts\View\View;
use SimKlee\LaravelBladeComponents\View\Components\AbstractComponent;

class CardTable extends AbstractComponent
{
    public string      $title;
    public string|null $pagination;

    public function __construct(string $title, string $pagination = null)
    {
        $this->title      = $title;
        $this->pagination = $pagination;
    }

    public function render(): View|Closure|string
    {
        return view('lbc::components.table.card-table');
    }

    public static function name(): string
    {
        return 'table.card-table';
    }
}
