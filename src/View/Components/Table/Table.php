<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\View\Components\Table;

use Closure;
use Illuminate\Contracts\View\View;
use SimKlee\LaravelBladeComponents\View\Components\AbstractComponent;

class Table extends AbstractComponent
{
    public function __construct()
    {

    }

    public function render(): View|Closure|string
    {
        return view('lbc::components.table.table');
    }

    public static function name(): string
    {
        return 'table.table';
    }
}
