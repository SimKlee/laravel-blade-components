<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents\View\Components\Table;

use Closure;
use Illuminate\Contracts\View\View;
use SimKlee\LaravelBladeComponents\View\Components\AbstractComponent;

class Header extends AbstractComponent
{
    public function render(): View|Closure|string
    {
        return view('lbc::components.table.header');
    }

    public static function name(): string
    {
        return 'table.header';
    }
}
