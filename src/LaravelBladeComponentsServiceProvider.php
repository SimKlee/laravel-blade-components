<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponents;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use SimKlee\LaravelBladeComponents\View\Components\AbstractComponent;
use SimKlee\LaravelBladeComponents\View\Components\Card\Card;
use SimKlee\LaravelBladeComponents\View\Components\Card\Body as CardBody;
use SimKlee\LaravelBladeComponents\View\Components\Card\Footer as CardFooter;
use SimKlee\LaravelBladeComponents\View\Components\Card\Header as CardHeader;
use SimKlee\LaravelBladeComponents\View\Components\Form\Form;
use SimKlee\LaravelBladeComponents\View\Components\Form\CardForm;
use SimKlee\LaravelBladeComponents\View\Components\Form\LabeledSelectbox;
use SimKlee\LaravelBladeComponents\View\Components\Form\LabeledSwitch;
use SimKlee\LaravelBladeComponents\View\Components\Form\LabeledTextbox;
use SimKlee\LaravelBladeComponents\View\Components\Model\Property;
use SimKlee\LaravelBladeComponents\View\Components\Table\CardTable;
use SimKlee\LaravelBladeComponents\View\Components\Table\Table;
use SimKlee\LaravelBladeComponents\View\Components\Table\Cell as TableCell;
use SimKlee\LaravelBladeComponents\View\Components\Table\Header as TableHeader;
use SimKlee\LaravelBladeComponents\View\Components\Table\Row as TableRow;
use SimKlee\LaravelBladeComponents\View\Components\ToggleButton;

class LaravelBladeComponentsServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        Paginator::useBootstrap();
        $this->bootBladeComponents();
    }

    public function register(): void
    {

    }

    private function bootBladeComponents(): void
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'lbc');
        $this->loadViewComponentsAs('', $this->getBladeComponentClasses()
                                             ->mapWithKeys(function (AbstractComponent|string $component) {
                                                 return ['lbc::' . $component::name() => $component];
                                             })->toArray()
        );
    }

    private function getBladeComponentClasses(): Collection
    {
        return collect([
            Form::class,
            CardForm::class,
            LabeledTextbox::class,
            LabeledSelectbox::class,
            LabeledSwitch::class,
            Property::class,
            Card::class,
            CardHeader::class,
            CardBody::class,
            CardFooter::class,
            Table::class,
            CardTable::class,
            TableHeader::class,
            TableRow::class,
            TableCell::class,
            ToggleButton::class,
        ]);
    }
}
